# README #

This module installs NLI-Hydra and all dependencies required to run the application

### Set up ###

Add the module reference to the Puppetfile of Puppet-Master in order to use this module 
```
mod 'nli-nli_hydra',
    :git => 'https://bitbucket.org/nlireland/puppet_module-nli_hydra'
```

### httpd.conf ###
*Note: This file is contained within the files directory. Additional configuration was hard coded below the heading 'Load config files in the "/etc/httpd/conf.d" directory, if any.' to allow passenger to work with apache.*