class nli_hydra (
  $ruby_version     = $nli_hydra::params::ruby_version,
  $app_name         = $nli_hydra::params::app_name,
  $deploy_dir = $nli_hydra::params::deploy_dir,
  $auth_enabled = $nli_hydra::params::auth_enabled,
  $auth_user_file = $nli_hydra::params::auth_user_file,
  $auth_user_name = $nli_hydra::params::auth_user_name,
  $auth_user_pass = $nli_hydra::params::auth_user_pass,
  $apache_servername = $nli_hydra::params::apache_servername,
  $apache_aliases = $nli_hydra::params::apache_aliases,
  $apache_port = $nli_hydra::params::apache_port,
  $root_ingested_folder = $nli_hydra::params::root_ingested_folder,
  $root_federated_url = $nli_hydra::params::root_federated_url,
  $metadata_service = $nli_hydra::params::metadata_service,
  $jpeg2k_cache = $nli_hydra::params::jpeg2k_cache,
  $jpeg2k_cache_device_ip = $nli_hydra::params::jpeg2k_cache_device_ip,
  $jpeg2k_cache_device_share = $nli_hydra::params::jpeg2k_cache_device_share,
  $jpeg2k_cache_mount_point = $nli_hydra::params::jpeg2k_cache_mount_point,
  $image_server_url = $nli_hydra::params::image_server_url,
  $system_notification_mail_from = $nli_hydra::params::system_notification_mail_from,
  $system_notification_mail_to = $nli_hydra::params::system_notification_mail_to,
  $db_adapter = $nli_hydra::params::db_adapter,
  $db_encoding = $nli_hydra::params::db_encoding,
  $db_database = $nli_hydra::params::db_database,
  $db_username = $nli_hydra::params::db_username,
  $db_password = $nli_hydra::params::db_password,
  $db_host = $nli_hydra::params::db_host,
  $db_port = $nli_hydra::params::db_port,
  $fedora_user = $nli_hydra::params::fedora_user,
  $fedora_password = $nli_hydra::params::fedora_password,
  $fedora_url = $nli_hydra::params::fedora_url,
  $fedora_base_path = $nli_hydra::params::fedora_base_path,
  $resque_host = $nli_hydra::params::resque_host,
  $resque_user = $nli_hydra::params::resque_user,
  $resque_password = $nli_hydra::params::resque_password,
  $solr_url = $nli_hydra::params::solr_url,
  $resque_pool_workers_datastream_queue = $nli_hydra::params::resque_pool_workers_datastream_queue,
  $resque_pool_workers_derivative_queue = $nli_hydra::params::resque_pool_workers_derivative_queue,
  $resque_pool_cron_required = $nli_hydra::params::resque_pool_cron_required,
  $max_tmp_file_age_minutes = $nli_hydra::params::max_tmp_file_age_minutes,
  $ldap_base = $nli_hydra::params::ldap_base,
  $kakadu_path = $nli_hydra::params::kakadu_path,
  $system_maintenance_hours = $nli_hydra::params::system_maintenance_hours,
) inherits nli_hydra::params {

  file  {"ingested_mount_dir":
    path => hiera('nli::ingested_mount_point'),
    ensure => "directory",
  }
  mount { "ingested_mount":
    name => hiera('nli::ingested_mount_point'),
    device  => hiera('nli::ingested_share'),
    fstype  => "nfs",
    ensure  => "mounted",
    options => "defaults",
    atboot  => true,
    require => [File["ingested_mount_dir"],Service['rpcbind']],
  }

  file  {"jp2_cache_mount_dir":
    path => $jpeg2k_cache_mount_point,
    ensure => "directory",
  }

  mount { "jp2_cache_mount":
    name => $jpeg2k_cache_mount_point,
    device  => "$jpeg2k_cache_device_ip:$jpeg2k_cache_device_share",
    fstype  => "nfs",
    ensure  => "mounted",
    options => "defaults",
    atboot  => true,
    require => [File["jp2_cache_mount_dir"],Service['rpcbind']],
  }

  service { 'rpcbind':
          ensure      => running,
          enable      => true,
          require     => Package["nfs-utils"];
  }

  include rvm

  rvm_system_ruby {
  "$ruby_version":
    ensure      => 'present',
    default_use => true;
  }

  # remove if update version of ruby to 2.2
  if $ruby_version == 'ruby-2.1.5' {
    package {'rack':
      ensure   => '1.6.4',
      provider => gem,
      before   => Class['passenger'],
    }
  }

  #### Capistrano
  include capistrano
  capistrano::deploytarget {$app_name:
    deploy_user    => $deploy_user,
    share_group    => $deploy_group,
    deploy_dir     => $deploy_dir,                 # default
    shared_dirs    => ['bin', 'bundle', 'config', 'public', 'config/initializers'], # default
  }

  file {'current':
    ensure      => link,
    path         => "$deploy_dir/current",
    target       => "$deploy_dir/releases/19700101000000",
    replace     => 'false',
    require      => File["blank_release"]
  }

  file {'blank_release':
    ensure       => directory,
    owner          => $deploy_user,
    path          => "$deploy_dir/releases/19700101000000",
    require      => Class["capistrano"]
  }

  # bundler needed by capistrano.
  # needs to be installed after rvm and via its own bash shell.
  # to be aware of rvm/ruby/gem settings.
  exec {'bundler':
    command => "bash -c 'source /etc/profile; gem install bundler'",
    logoutput => 'true',
    path => 'usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin',
    creates => "/usr/local/rvm/gems/$ruby_version/bin/bundler",
    require => Class['rvm'],
  }

  # git needed by capistrano
  include git

  # config files for the app
  file { 'database.yml':
    ensure  => present,
    owner   => $deploy_user,
    group   => $deploy_group,
    path     => "$deploy_dir/shared/config/database.yml",
    content => template('nli_hydra/database.yml.erb'),
    require => Class["capistrano"]
  }
  file { 'config.yml':
    ensure  => present,
    owner   => $deploy_user,
    group   => $deploy_group,
    path     => "$deploy_dir/shared/config/config.yml",
    content => template('nli_hydra/config.yml.erb'),
    require => Class["capistrano"]
  }
  file { 'fedora.yml':
    ensure  => present,
    owner   => $deploy_user,
    group   => $deploy_group,
    path     => "$deploy_dir/shared/config/fedora.yml",
    content => template('nli_hydra/fedora.yml.erb'),
    require => Class["capistrano"]
  }
  file { 'ldap.yml':
    ensure  => present,
    owner   => $deploy_user,
    group   => $deploy_group,
    path     => "$deploy_dir/shared/config/ldap.yml",
    content => template('nli_hydra/ldap.yml.erb'),
    require => Class["capistrano"]
  }
  file { 'resque.yml':
    ensure  => present,
    owner   => $deploy_user,
    group   => $deploy_group,
    path     => "$deploy_dir/shared/config/resque.yml",
    content => template('nli_hydra/resque.yml.erb'),
    require => Class["capistrano"]
  }
  file { 'resque-pool.yml':
    ensure  => present,
    owner   => $deploy_user,
    group   => $deploy_group,
    path     => "$deploy_dir/shared/config/resque-pool.yml",
    content => template('nli_hydra/resque-pool.yml.erb'),
    require => Class["capistrano"]
  }
  file { 'solr.yml':
    ensure  => present,
    owner   => $deploy_user,
    group   => $deploy_group,
    path     => "$deploy_dir/shared/config/solr.yml",
    content => template('nli_hydra/solr.yml.erb'),
    require => Class["capistrano"]
  }
  file { 'role_map.yml':
    ensure  => present,
    owner   => $deploy_user,
    group   => $deploy_group,
    path     => "$deploy_dir/shared/config/role_map.yml",
    content => template('nli_hydra/role_map.yml.erb'),
    require => Class["capistrano"]
  }


  #passenger & dependencies
  $required_packages = ['gcc-c++', 'ruby-devel', 'apr-devel', 'apr-util-devel', 'mariadb-devel', 'mariadb', 'libxml2-devel', 'libxslt-devel', 'nfs-utils', 'libnfsidmap']
  # the 'unzip' package is installed by kakadu

  # Using puppetlabs-passenger because mod_passenger is not yet available from epel.
  # TODO: revert to using normal apache module once mod_passenger is available.
  class {'passenger':
    package_ensure         => '4.0.59',
    passenger_version      => '4.0.59',
    passenger_provider     => 'gem',
    passenger_package      => 'passenger',
    gem_path               => '/usr/local/share/gems/gems',
    gem_binary_path        => '/usr/local/bin',
    passenger_root         => '/usr/local/share/gems/gems/passenger-4.0.59',
    passenger_ruby         => "/usr/local/rvm/gems/$ruby_version/wrappers/ruby",
    passenger_app_env  =>  $environment,
    mod_passenger_location => '/usr/local/share/gems/gems/passenger-4.0.59/buildout/apache2/mod_passenger.so',
    require                => [Package[$required_packages, 'unzip'], Apache::Vhost[$apache_servername]],
  }

  package {$required_packages:
    ensure => 'installed',
    install_options => ['--enablerepo=rhel-7-server-optional-rpms'],
  }

  #### Apache Vhost and dependencies
  # Note: to avoid duplicate class declaration, we have forked puppetlabs-passenger
  # and removed the include from init.pp. This is necessary to provide params to
  # the apache module, otherwise apache is installed with all the defaults
  class { 'apache':
    default_mods           => false,
    default_confd_files    => false,
    mpm_module             => 'event',
  }

  class{ '::apache::dev':
    require => Class['apache'],
  }

  # enable mod_headers
  apache::mod { 'headers': }

  # enable mime type checking
  apache::mod { 'mime': }
  apache::mod { 'mime_magic': }
  apache::custom_config { 'mime-types':
    content => 'TypesConfig /etc/mime.types',
  }

  # mod_dir
  apache::mod { 'dir': }

  # enable mod_deflate
  class { '::apache::mod::deflate':
    types => [ 'text/html',
               'text/plain',
               'text/xml',
               'text/css',
               'application/json',
               'application/xml',
               'application/x-javascript',
               'application/javascript',
               'application/ecmascript',
               'application/rss-xml',
               'application/font-woff',
             ],
  }



  # Assign directory and location blocks into local variables to dry out vhost block
  # Public <Direcotory />
  $vhost_directory = {
    path           => "$deploy_dir/current/public",
    allow_override => ['None'],
  }

  # Assets <Location />. Set far future cache control
  $vhost_location_assets = {
     path          => "/assets/",
     provider => 'location',
     headers => 'Set Cache-Control "public,max-age=31536000"',
   }

  # Root <Location /> with optional basic authentication
  if $auth_enabled {
    $vhost_location_root = {
         path          => "/",
         provider => 'location',
         auth_type     => 'Basic',
         auth_name     => 'Please login',
         auth_user_file => $auth_user_file,
         auth_require  => "user $auth_user_name",
       }
    # Gather up the various directory and location blocaks.
    $vhost_path_settings = [$vhost_directory, $vhost_location_root, $vhost_location_assets,]
  } else {
    # Gather up the various directory and location blocaks.
    $vhost_path_settings = [$vhost_directory, $vhost_location_assets,]
  }

  # Setup up the primary vhost
  apache::vhost { $apache_servername:
    port           => $apache_port,
    docroot        => "$deploy_dir/current/public",
    docroot_owner  => $deploy_user,
    directories    => $vhost_path_settings,
    serveraliases  => ["${ipaddress}",],
    require        => [File['current'], Class['apache'],],
  }

  # Add a user file if we're using basic authentication and
  # enable necessary apache modules
  if $auth_enabled {
    # enable required apache modules
    apache::mod { 'auth_basic': }
    apache::mod { 'authn_core': }
    apache::mod { 'authn_file': }
    apache::mod { 'authz_user': }

    httpauth { $auth_user_name:
      file     => $auth_user_file,
      password => $auth_user_pass,
      ensure => present,
      require      => [File['current'], Apache::Vhost[$apache_servername]],
    }->
    file { $auth_user_file:
      mode => 644
    }
  }

  class { 'selinux':
    mode => 'permissive'
  }

  #delete tmp files left by mini_magick/sufia
  file {'/usr/bin/clear_hydra_tmp_cron.sh':
    ensure => 'present',
    owner => 'root',
    group => 'root',
    mode => '0755',
    content => template('nli_hydra/tmp_cron.sh.erb'),
  }
  cron { 'clean_hydra_tmp':
    command => "sh /usr/bin/clear_hydra_tmp_cron.sh",
    user    => root,
    hour    => 'absent',
    minute  => 'absent',
    require => File['/usr/bin/clear_hydra_tmp_cron.sh'],
  }

  #other libraries which hydra depends on to generate derivatives
  package { 'ImageMagick':
    name => 'ImageMagick',
    ensure => 'installed',
  }

  class{'nli_kakadu':
  }

  #resque-pool
  file {"/var/www/$app_name/shared/resque-pool.sh":
    ensure  => file,
    owner  => 'puppetadmin',
    group  => 'puppetadmin',
    mode   => '0755',
    content => template('nli_hydra/resque-pool.erb'),
  }
  file {"/etc/init.d/resque-pool-initd":
    ensure  => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    content => template('nli_hydra/resque-pool-initd.erb'),
    require  => File["/var/www/$app_name/shared/resque-pool.sh"],
  }
  service { 'resque-pool-initd' :
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
    require => File["/etc/init.d/resque-pool-initd"],
  }

  if $resque_pool_cron_required == true {

    cron {'shutdown_resque':
      command => "sh /etc/init.d/resque-pool-initd stop",
      user    => root,
      hour    => '5',
      minute  => '0',
      require => File["/etc/init.d/resque-pool-initd"],
    }
    cron {'startup_resque':
      command => "sh /etc/init.d/resque-pool-initd start",
      user    => root,
      hour    => '5',
      minute  => '10',
      require => File["/etc/init.d/resque-pool-initd"],
    }
  }
}
